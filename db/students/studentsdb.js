const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('students.json');
const db = low(adapter);

db.defaults({ students: [] }).write();

module.exports = db;
