const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('notes.json');
const db = low(adapter);

db.defaults({ notes: [] }).write();

module.exports = db;
