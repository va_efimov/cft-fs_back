const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('lessons.json');
const db = low(adapter);

db.defaults({ lessons: [] }).write();

module.exports = db;
