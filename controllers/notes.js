const shortid = require('shortid');
const { validate } = require('jsonschema');
const db = require('../db/notes/notesdb');

const getNotes = (req, res, next) => {
  let notes = [];
  try {
    notes = db.get('notes');
  } catch (error) {
    throw new Error(error);
  }
  res.json({ status: 'OK', data: notes });
};

const getNote = (req, res, next) => {
  const { id } = req.params;

  const note = db
    .get('notes')
    .find({ id })
    .value();

  if (!note) {
    throw new Error('NOTE_NOT_FOUND');
  }

  res.json({ status: 'OK', data: note });
};

const createNote = (req, res, next) => {
  const noteSchema = {
    type: 'object',
    properties: {
      heading: { type: 'string' },
      author: { type: 'string' },
      date: { type: 'string' },
      description: { type: 'string' }
    },
    required: ['heading', 'description'],
    additionalProperties: false
  };

  const validationResult = validate(req.body, noteSchema);
  if (!validationResult.valid) {
    throw new Error('INVALID_JSON_OR_API_FORMAT');
  }

  const {
    heading,
    author,
    description
  } = req.body;

  const dateNow = new Date().toLocaleDateString();

  const note = {
    id: shortid.generate(),
    date: dateNow,
    heading,
    author,
    description
  };

  try {
    db.get('notes')
      .push(note)
      .write();
  } catch (error) {
    throw new Error(error);
  }

  res.json({
    status: 'OK',
    data: note
  });
};

const editNote = (req, res, next) => {
  const { id } = req.params;

  const editedNote = db
    .get('notes')
    .find({ id })
    .assign(req.body)
    .value();

  db.write();

  res.json({ status: 'OK', data: editedNote });
};

const deleteNote = (req, res, next) => {
  db.get('notes')
    .remove({ id: req.params.id })
    .write();

  res.json({ status: 'OK' });
};

module.exports = {
  getNotes,
  getNote,
  createNote,
  editNote,
  deleteNote
};
