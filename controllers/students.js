const shortid = require('shortid');
const { validate } = require('jsonschema');
const db = require('../db/students/studentsdb');

const getStudents = (req, res, next) => {
  let students = [];
  try {
    students = db.get('students');
  } catch (error) {
    throw new Error(error);
  }
  res.json({ status: 'OK', data: students });
};

const getStudent = (req, res, next) => {
  const { id } = req.params;

  const student = db
    .get('students')
    .find({ id })
    .value();

  if (!student) {
    throw new Error('STUDENT_NOT_FOUND');
  }

  res.json({ status: 'OK', data: student });
};

const createStudent = (req, res, next) => {
  const studentSchema = {
    type: 'object',
    properties: {
      name: { type: 'string' },
      city: { type: 'string' },
      status: { type: 'bool' },
      age: { type: 'string' },
      description: { type: 'string' },
      photo: { type: 'string' },
    },
    required: ['name', 'status'],
    additionalProperties: false
  };

  const validationResult = validate(req.body, studentSchema);
  if (!validationResult.valid) {
    throw new Error('INVALID_JSON_OR_API_FORMAT');
  }

  const {
    name,
    status,
    age,
    description
  } = req.body;

  const student = {
    id: shortid.generate(),
    name,
    status: JSON.parse(status),
    age,
    description
  };

  try {
    db.get('students')
      .push(student)
      .write();
  } catch (error) {
    throw new Error(error);
  }

  res.json({
    status: 'OK',
    url: `/students/${student.id}`,
    data: student
  });
};

const editStudent = (req, res, next) => {
  const { id } = req.params;

  const editedStudent = db
    .get('students')
    .find({ id })
    .assign(req.body)
    .value();

  db.write();

  res.json({ status: 'OK', data: editedStudent });
};

const deleteStudent = (req, res, next) => {
  db.get('students')
    .remove({ id: req.params.id })
    .write();

  res.json({ status: 'OK' });
};

module.exports = {
  getStudents,
  getStudent,
  createStudent,
  editStudent,
  deleteStudent
};
