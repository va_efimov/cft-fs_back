const shortid = require('shortid');
const { validate } = require('jsonschema');
const db = require('../db/lessons/lessonsdb');

const getLessons = (req, res, next) => {
  let lessons = [];
  try {
    lessons = db.get('lessons');
  } catch (error) {
    throw new Error(error);
  }
  res.json({ status: 'OK', data: lessons });
};

const getLesson = (req, res, next) => {
  const { id } = req.params;

  const lesson = db
    .get('lessons')
    .find({ id })
    .value();

  if (!lesson) {
    throw new Error('LESSON_NOT_FOUND');
  }

  res.json({ status: 'OK', data: lesson });
};

/*
const getNextLesson = (req, res, next) => {

  const today = new Date();
  const NY = new Date('1/1/2015');
  console.log(today);
  const milliseconds = today - NY;
  const seconds = milliseconds / 1000;
  const minutes = seconds / 60;
  const hours = minutes / 60;
  const days = hours / 24;
  console.log(days);

  let lessons = [];
  try {
    lessons = db.get('lessons');
  } catch (error) {
    throw new Error(error);
  }

  let lessonId;
  let delta = 1000;

  lessons.forEach((lesson) => {
    let dateLesson = new Date(lesson.date);
    dateLesson = (dateLesson / 1000 / 60 / 60 / 24) - NY;
    if ((dateLesson - days) > 0) {
      if ((dateLesson - days) < delta) {
        delta = dateLesson - days;
        lessonId = lesson.id;
      }
    }
  });

  const lesson = db
    .get('lessons')
    .find(lessonId)
    .value();

  if (!lesson) {
    throw new Error('LESSON_NOT_FOUND');
  }

  res.json({ status: 'OK', data: lesson });
};
*/

const createLesson = (req, res, next) => {
  const lessonSchema = {
    type: 'object',
    properties: {
      theme: { type: 'string' },
      date: { type: 'string' },
      section: { type: 'string' },
      resource: { type: 'string' },
      video: { type: 'string' },
      description: { type: 'string' }
    },
    required: ['theme', 'date', 'section'],
    additionalProperties: false
  };

  const validationResult = validate(req.body, lessonSchema);
  if (!validationResult.valid) {
    throw new Error('INVALID_JSON_OR_API_FORMAT');
  }

  const {
    theme,
    date,
    section,
    resource,
    video,
    description
  } = req.body;

  const lesson = {
    id: shortid.generate(),
    theme,
    date,
    section,
    resource,
    video,
    description
  };

  try {
    db.get('lessons')
      .push(lesson)
      .write();
  } catch (error) {
    throw new Error(error);
  }

  res.json({
    status: 'OK',
    url: `/lessons/${lesson.id}`,
    data: lesson
  });
};

const editLesson = (req, res, next) => {
  const { id } = req.params;

  const editedLesson = db
    .get('lessons')
    .find({ id })
    .assign(req.body)
    .value();

  db.write();

  res.json({ status: 'OK', data: editedLesson });
};

const deleteLesson = (req, res, next) => {
  db.get('lessons')
    .remove({ id: req.params.id })
    .write();

  res.json({ status: 'OK' });
};

module.exports = {
  getLessons,
  getLesson,
  createLesson,
  editLesson,
  deleteLesson
};
