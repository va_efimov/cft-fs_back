const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();

const studentsRotues = require('./routes/students');
const notesRotues = require('./routes/notes');
const lessonsRoutes = require('./routes/lessons');

app.use(morgan('combined'));
app.use(bodyParser.json()); // application/json

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.get('/', (req, res, next) => {
  res.send('LogIn page');
});

app.use('/students', studentsRotues);
app.use('/notes', notesRotues);
app.use('/lessons', lessonsRoutes);

// Error handling
app.use((err, req, res, next) => {
  const { message } = err;
  res.json({ status: 'ERROR', message });
});

app.listen(8060);
