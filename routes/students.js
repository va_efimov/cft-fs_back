const express = require('express');

const router = express.Router();

const tasksController = require('../controllers/students');

// GET /students
router.get('/', tasksController.getStudents);

// GET /students/:id
router.get('/:id', tasksController.getStudent);

// POST /students
router.post('/', tasksController.createStudent);

// PATCH /students/:id
router.patch('/:id', tasksController.editStudent);

// DELETE /students/:id
router.delete('/:id', tasksController.deleteStudent);

module.exports = router;
