const express = require('express');

const router = express.Router();

const tasksController = require('../controllers/lessons');

// GET /lessons
router.get('/', tasksController.getLessons);

// GET /lessons/:id
router.get('/:id', tasksController.getLesson);

// POST /lessons
router.post('/', tasksController.createLesson);

// PATCH /lessons/:id
router.patch('/:id', tasksController.editLesson);

// DELETE /lessons/:id
router.delete('/:id', tasksController.deleteLesson);

module.exports = router;
