const express = require('express');

const router = express.Router();

const tasksController = require('../controllers/notes');

// GET /notes
router.get('/', tasksController.getNotes);

// GET /notes/:id
router.get('/:id', tasksController.getNote);

// POST /notes
router.post('/', tasksController.createNote);

// PATCH /notes/:id
router.patch('/:id', tasksController.editNote);

// DELETE /notes/:id
router.delete('/:id', tasksController.deleteNote);

module.exports = router;
